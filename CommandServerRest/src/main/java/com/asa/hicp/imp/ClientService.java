package com.asa.hicp.imp;

import java.io.IOException;
import java.util.ArrayList;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.asa.hicp.model.Book;
import com.asa.hicp.model.Client;
import com.asa.hicp.model.Commande;

@Produces(MediaType.APPLICATION_JSON)
public interface ClientService {
	@GET
	@Path("/allClients")
	public ArrayList<Client> getAll();

	@GET
	@Path("/{id}")
	public Client getClient(@PathParam("id") Long id);

	@PUT
	@Path("/{id}")
	public void updateClient(@PathParam("id") Long id, Client client);

	@POST
	@Path("/addClient")
	public void addClient(Client client);

	@DELETE
	@Path("/{id}")
	public void deleteClient(@PathParam("id") Long id);

	@GET
	@Path("/allCommands")
	public ArrayList<Commande> getAllCommands();

	@POST
	@Path("/addCommand")
	public void addCommande(Commande commande);

	@GET
	@Path("/getCommand/{id}")
	public Commande getCommande(@PathParam("id") Long id);

	@PUT
	@Path("/updateCommand/{id}")
	public void updateCommande(@PathParam("id") Long id, Commande commande);

	@DELETE
	@Path("/deleteCommande/{id}")
	public void deleteCommande(@PathParam("id") Long id);

	@POST
	@Path("/addAndReturnCommande")
	public Commande addAndReturnCommande(Commande commande);

	@POST
	@Path("/writeExcel")
	public void writeExcell() throws IOException;

	@POST
	@Path("/readExcel")
	public void readExcell() throws IOException;

	@GET
	@Path("/allBooks")
	public ArrayList<Book> getAllBooks();

}
