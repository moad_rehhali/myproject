package com.asa.hicp.imp;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.asa.hicp.model.Article;
import com.asa.hicp.model.Categorie;
import com.asa.hicp.model.Fournisseur;

@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class ArticleServiceImpl implements ArticleService {

	@PersistenceContext(unitName = "testunit")
	private EntityManager em;

	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	public EntityManager getEntityManager() {
		return em;
	}

	@Override
	public ArrayList<Article> getAllArticles() {
		return (ArrayList<Article>) em.createQuery("select a from Article a", Article.class).getResultList();
	}

	@Override
	public Article getArticle(Long id) {
		return em.find(Article.class, id);
	}

	@Override
	@Transactional
	public void updateArticle(Long id, Article article) {
		Article article1 = em.find(Article.class, id);
		article1.setFournisseur(article.getFournisseur());
		article1.setCom(article.getCom());
		article1.setDesignation(article.getDesignation());
		article1.setPrix(article.getPrix());
		article1.setCategorie(article.getCategorie());
		em.persist(article1);
	}

	@Override
	public void addArticle(Article article) {
		em.persist(article);
	}

	@Override
	public void deleteArticle(Long id) {
		Article article = em.find(Article.class, id);
		em.remove(article);
	}

	@Override
	public ArrayList<Fournisseur> getAllFournisseurs() {
		return (ArrayList<Fournisseur>) em.createQuery("select f from Fournisseur f", Fournisseur.class)
				.getResultList();
	}

	@Override
	public ArrayList<Categorie> getAllCategories() {
		return (ArrayList<Categorie>) em.createQuery("select c from Categorie c", Categorie.class).getResultList();
	}

	@Override
	public Categorie getCategorie(Long id) {
		return em.find(Categorie.class, id);
	}

	@Override
	public Fournisseur getFournisseur(Long id) {
		return em.find(Fournisseur.class, id);
	}

}
