package com.asa.hicp.imp;

import java.util.ArrayList;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.asa.hicp.model.Article;
import com.asa.hicp.model.Categorie;
import com.asa.hicp.model.Fournisseur;

@Produces(MediaType.APPLICATION_JSON)
public interface ArticleService {
	@GET
	@Path("/allArticles")
	public ArrayList<Article> getAllArticles();

	@GET
	@Path("/{id}")
	public Article getArticle(@PathParam("id") Long id);

	@PUT
	@Path("/{id}")
	public void updateArticle(@PathParam("id") Long id, Article article);

	@POST
	@Path("/addArticle")
	public void addArticle(Article article);

	@DELETE
	@Path("/{id}")
	public void deleteArticle(@PathParam("id") Long id);

	@GET
	@Path("/allFournisseurs")
	public ArrayList<Fournisseur> getAllFournisseurs();

	@GET
	@Path("/getFournisseur/{id}")
	public Fournisseur getFournisseur(@PathParam("id") Long id);

	@GET
	@Path("/allCategories")
	public ArrayList<Categorie> getAllCategories();

	@GET
	@Path("/getCategorie/{id}")
	public Categorie getCategorie(@PathParam("id") Long id);

}
