/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.asa.hicp.imp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.asa.hicp.model.Book;
import com.asa.hicp.model.Client;
import com.asa.hicp.model.Commande;

public class ClientServiceImpl implements ClientService {

	@PersistenceContext(unitName = "testunit")
	// PersistenceContextType.EXTENDED)
	private EntityManager em;

	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	public EntityManager getEntityManager() {
		return em;
	}

	@Override
	public ArrayList<Client> getAll() {
		return (ArrayList<Client>) em.createQuery("select e from Client e", Client.class).getResultList();
	}

	@Override
	public Client getClient(Long id) {
		// return em.createQuery("select c from Client c where c.id=" + id,
		// Client.class).getSingleResult();
		return em.find(Client.class, id);
	}

	@Override
	@Transactional
	public void updateClient(Long id, Client client) {
		Client client1 = em.find(Client.class, id);
		client1.setNom(client.getNom());
		client1.setAdr(client.getAdr());
		client1.setEmail(client.getEmail());
		client1.setPrenom(client.getPrenom());
		client1.setTel(client.getTel());
		em.persist(client1);
		// em.merge(client);
	}

	@Override
	@Transactional
	public void addClient(Client client) {
		em.persist(client);
	}

	@Override
	@Transactional
	public void deleteClient(Long id) {
		Client client = em.find(Client.class, id);
		System.out.println(client.getNom());
		em.remove(client);
	}

	@Override
	public ArrayList<Commande> getAllCommands() {
		return (ArrayList<Commande>) em.createQuery("select c from Commande c", Commande.class).getResultList();
	}

	@Override
	@Transactional
	public void addCommande(Commande commande) {
		em.persist(commande);
	}

	@Override
	public Commande getCommande(Long id) {
		return em.find(Commande.class, id);
	}

	@Override
	@Transactional
	public void updateCommande(Long id, Commande commande) {
		Commande cm = em.find(Commande.class, id);
		cm.setNum(commande.getNum());
		cm.setQte(commande.getQte());
		cm.setDate(commande.getDate());
		cm.setClient(commande.getClient());
		em.persist(cm);
	}

	@Override
	@Transactional
	public void deleteCommande(Long id) {
		Commande commande = em.find(Commande.class, id);
		em.remove(commande);
	}

	@Override
	@Transactional
	public Commande addAndReturnCommande(Commande commande) {
		em.persist(commande);
		return commande;
	}

	@Override
	public ArrayList<Book> getAllBooks() {
		return (ArrayList<Book>) em.createQuery("select e from Book e", Book.class).getResultList();
	}

	// --------READ FROM THE DATABASE AND WRITE DATA IN AN EXCELL FILE
	// ---------------

	@Override
	@Transactional
	public void writeExcell() throws IOException {

		List<Book> listBook = getAllBooks();
		String excelFilePath = "E:/free/NiceJavaBooks.xls";

		// try {
		writeExcel(listBook, excelFilePath);
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
	}

	private void writeBook(Book aBook, Row row) {
		Cell cell = row.createCell(1);
		cell.setCellValue(aBook.getTitle());

		cell = row.createCell(2);
		cell.setCellValue(aBook.getAuthor());

		cell = row.createCell(3);
		cell.setCellValue(aBook.getPrice());
	}

	public void writeExcel(List<Book> listBook, String excelFilePath) throws IOException {
		Workbook workbook = new HSSFWorkbook();
		Sheet sheet = workbook.createSheet();

		int rowCount = 0;
		// create get all books
		for (Book aBook : listBook) {
			Row row = sheet.createRow(++rowCount);
			writeBook(aBook, row);
		}

		FileOutputStream outputStream = new FileOutputStream(excelFilePath);
		workbook.write(outputStream);
	}

	//
	// public List<Book> getListBook() {
	// Book book1 = new Book("Head First Java", "Kathy Serria & moad rehhali",
	// 79.0);
	// Book book2 = new Book("Effective Java", "Joshua Bloch", 36.0);
	// Book book3 = new Book("Clean Code", "Robert Martin", 42.0);
	// Book book4 = new Book("Thinking in Java", "Bruce Eckel", 35.0);
	//
	// List<Book> listBook = Arrays.asList(book1, book2, book3, book4);
	//
	// return listBook;
	// }
	//

	// -----------READ FROM AN EXCELL FILE AND INSERT DATA INTO THE
	// DATABASE-------------------
	@Override
	@Transactional
	public void readExcell() throws IOException {

		String excelFilePath = "E:/Applications/e4-AsaWorkspace/commandeRest/CommandServerRest/NiceJavaBooks.xls";
		List<Book> listBooks = readBooksFromExcelFile(excelFilePath);
		for (Book bo : listBooks) {
			System.out.println("Title : " + bo.getTitle());

			Book book = new Book(bo.getTitle(), bo.getAuthor(), bo.getPrice());
			em.persist(book);
		}
	}

	private Object getCellValue(Cell cell) {
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			return cell.getStringCellValue();

		case Cell.CELL_TYPE_BOOLEAN:
			return cell.getBooleanCellValue();

		case Cell.CELL_TYPE_NUMERIC:
			return cell.getNumericCellValue();
		}

		return null;
	}

	public List<Book> readBooksFromExcelFile(String excelFilePath) throws IOException {
		List<Book> listBooks = new ArrayList<Book>();
		FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

		Workbook workbook = new HSSFWorkbook(inputStream);
		Sheet firstSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = firstSheet.iterator();

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			Book book = new Book();

			while (cellIterator.hasNext()) {
				Cell nextCell = cellIterator.next();
				int columnIndex = nextCell.getColumnIndex();

				switch (columnIndex) {
				case 1:
					book.setTitle((String) getCellValue(nextCell));
					break;
				case 2:
					book.setAuthor((String) getCellValue(nextCell));
					break;
				case 3:
					book.setPrice((Double) getCellValue(nextCell));
					break;
				}
			}
			listBooks.add(book);
		}
		inputStream.close();

		return listBooks;
	}

}
