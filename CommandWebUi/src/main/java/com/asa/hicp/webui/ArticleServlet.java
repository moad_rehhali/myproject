package com.asa.hicp.webui;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.asa.hicp.imp.ArticleService;
import com.asa.hicp.imp.ClientService;
import com.asa.hicp.model.Article;
import com.asa.hicp.model.Categorie;
import com.asa.hicp.model.Fournisseur;

public class ArticleServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private ArticleService articleService;
	private ClientService clientService;
	String update_id;
	String delete_id;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		ServletOutputStream os = resp.getOutputStream();
		ArrayList<Article> articles = articleService.getAllArticles();

		update_id = req.getParameter("update_id");
		delete_id = req.getParameter("delete_id");

		if (delete_id != null) {
			articleService.deleteArticle(Long.parseLong(delete_id));
			resp.sendRedirect("/articleuirest");
		}

		os.println("<html><body>");
		os.println("<h1>Liste des articles</h1>");
		os.println("<table>");

		os.println("<tr><th>ID</th><th>Designation</th><th>Prix</th><th>Categorie</th><th>Fournisseur</th></tr>");
		for (Article ar : articles) {
			os.println(String.format(
					"<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td><a href='?update_id=" + ar.getId()
							+ "'>Edit</a></td><td><a href='?delete_id=" + ar.getId() + "'>Delete</a></td></tr>",
					ar.getId(), ar.getDesignation(), ar.getPrix(), ar.getCategorie().getNom(),
					ar.getFournisseur().getNom() + " " + ar.getFournisseur().getPrenom()

			));
		}
		os.println("</table>");

		if (update_id == null) {
			os.println("<br><br>");
			os.println("<h2>Ajouter un article</h2>");
			os.println("<form name='input' action='/articleuirest' method='post'>");
			os.println("<table>");
			os.println("<tr><td>Désignation</td><td><input type='text' name='designation'/></td></tr>");
			os.println("<tr><td>Prix</td><td><input type='text' name='prix'/></td></tr>");
			// os.println(
			// "<tr><td>Commande</td><td><select name='commande'> <option
			// value='hidden'>Choisit une commande</option>");
			// for (Commande cm : clientService.getAllCommands()) {
			// os.println(String.format(" <option value=%s >%s</option>",
			// cm.getId(), cm.getNum()));
			// }
			// os.println("</select></tr>");
			os.println(
					"<tr><td>Catégorie</td><td><select  name='categorie'> <option value='hidden'>Choisit une categorie</option>");
			for (Categorie cg : articleService.getAllCategories()) {
				os.println(String.format(" <option value=%s >%s</option>", cg.getId(), cg.getNom()));
			}
			os.println("</select></tr>");
			os.println(
					"<tr><td>Fournisseur</td><td><select  name='fournisseur'> <option value='hidden'>Choisit un fournisseur</option>");
			for (Fournisseur fo : articleService.getAllFournisseurs()) {
				os.println(String.format(" <option value=%s >%s</option>", fo.getId(), fo.getNom()));
			}
			os.println("</select></tr>");
			os.println("<input type='hidden' name='hidden' value='addArticle' />");
			os.println("<tr><td colspan='2'><input type='submit' value='Add' /></td></tr>");
			os.println("</table>");
			os.println("</form>");

		} else {

			Article ar = articleService.getArticle(Long.parseLong(update_id));
			os.println("<br><br>");
			os.println("<h2>Modifier un article</h2>");
			os.println("<form name='input' action='/articleuirest' method='post'>");
			os.println("<table>");
			os.println("<tr><td>Désignation</td><td><input type='text' name='designation' value='" + ar.getDesignation()
					+ "'/></td></tr>");
			os.println("<tr><td>Prix</td><td><input type='text' name='prix' value='" + ar.getPrix() + "'/></td></tr>");
			os.println("<tr><td>Catégorie</td><td><select  name='categorie'>  <option value='"
					+ ar.getCategorie().getId() + "'>" + ar.getCategorie().getNom() + "</option>");
			for (Categorie cg : articleService.getAllCategories()) {
				if (cg.getId() != ar.getCategorie().getId()) {
					os.println(String.format(" <option value=%s >%s</option>", cg.getId(), cg.getNom()));
				}
			}
			os.println("</select></tr>");
			os.println("<tr><td>Fournisseur</td><td><select  name='fournisseur'> <option value='"
					+ ar.getFournisseur().getId() + "'>" + ar.getFournisseur().getNom() + "</option>");
			for (Fournisseur fo : articleService.getAllFournisseurs()) {
				if (fo.getId() != ar.getFournisseur().getId()) {
					os.println(String.format(" <option value=%s >%s</option>", fo.getId(), fo.getNom()));
				}
			}
			os.println("</select></tr>");
			os.println("<input type='hidden' name='hidden' value='editArticle' />");
			os.println(
					"<tr><td colspan='2'><input type='submit' value='Edit' id='editArticle'/></td><td><input type='hidden' name='update_id' value='"
							+ update_id + " '  /></td></tr>");
			os.println("</table>");
			os.println("</form>");

		}

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String hidden = req.getParameter("hidden");
		String designation = req.getParameter("designation");
		String prix = req.getParameter("prix");
		String cat = req.getParameter("categorie");
		String four = req.getParameter("fournisseur");

		if (hidden.equals("addArticle")) {

			Article article = new Article(designation, Double.parseDouble(prix),
					articleService.getCategorie(Long.parseLong(cat)),
					articleService.getFournisseur(Long.parseLong(four)));
			articleService.addArticle(article);
		} else if (hidden.equals("editArticle")) {

			// Article art =
			// articleService.getArticle(Long.parseLong(update_id));

			articleService.updateArticle(Long.parseLong(update_id),
					new Article(designation, Double.parseDouble(prix), articleService.getCategorie(Long.parseLong(cat)),
							articleService.getFournisseur(Long.parseLong(four))));

		}

		resp.sendRedirect("articleuirest");
	}

	public ArticleService getArticleService() {
		return articleService;
	}

	public void setArticleService(ArticleService articleService) {
		this.articleService = articleService;
	}

	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}
}
