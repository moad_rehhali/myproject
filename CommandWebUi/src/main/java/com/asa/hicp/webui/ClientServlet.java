package com.asa.hicp.webui;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.asa.hicp.imp.ClientService;
import com.asa.hicp.model.Client;
import com.asa.hicp.model.Commande;

public class ClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ClientService clientService;
	String update_id;
	String delete_id;
	String pattern = "dd-MM-yyyy";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	String pattern1 = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ServletOutputStream os = resp.getOutputStream();
		ArrayList<Client> clients = clientService.getAll();
		ArrayList<Commande> commandes = clientService.getAllCommands();

		update_id = req.getParameter("update_id");
		delete_id = req.getParameter("delete_id");

		if (delete_id != null) {
			clientService.deleteCommande(Long.parseLong(delete_id));
			resp.sendRedirect("/clientuirest");
		}

		os.println("<html><body>");
		os.println("<h1>Liste des clients</h1>");
		os.println("<table>");
		os.println("<tr><th>Id</th><th>Name</th><th>Prenom</th><th>Email</th></tr>");
		for (Client client : clients) {
			os.println(String.format(
					"<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td><input type='submit' value='Edit' id='%s'/></td><td><input type='submit' value='Delete'/></td></tr>",
					client.getId(), client.getNom(), client.getPrenom(), client.getEmail(), client.getId()));
		}
		os.println("</table>");
		os.println("<br><br>");
		os.println("<h2>Ajouter un client</h2>");
		os.println("<form name='input' action='/clientuirest' method='post'>");
		os.println("<table>");
		os.println("<tr><td>NomClient</td><td><input type='text' name='nom' id='nom'/></td></tr>");
		os.println("<tr><td>PrenomClient</td><td><input type='text' name='prenom' id='prenom'/></td></tr>");
		os.println("<tr><td>EmailClient</td><td><input type='text' name='email' id='email'/></td></tr>");
		os.println("<input type='hidden' name='hidden' value='addClient' />");
		os.println("<tr><td colspan='2'><input type='submit' value='Add' id='addClient'/></td></tr>");
		os.println("</table>");
		os.println("</form>");

		os.println("<br><br>");
		os.println("<h1>Liste des commandes</h1>");
		os.println("<table>");

		os.println("<tr><th>ID</th><th>Num</th><th>QTE</th><th>DATE</th><th>CLIENT</th></tr>");
		for (Commande cm : commandes) {
			String date = simpleDateFormat.format(cm.getDate());
			os.println(String.format(
					"<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td><a href='?update_id=" + cm.getId()
							+ "'>Edit</a></td><td><a href='?delete_id=" + cm.getId() + "'>Delete</a></td></tr>",
					cm.getId(), cm.getNum(), cm.getQte(), date.replace("-", "/"), cm.getClient().getNom()

			));
		}
		os.println("</table>");

		os.println("<br><br>");
		if (update_id == null) {
			os.println("<h2>Ajouter une commande</h2>");

			os.println("<form name='input' action='/clientuirest' method='post'>");
			os.println("<table>");

			os.println("<tr></tr><tr></tr>");
			os.println("<tr><td>Num</td><td><input type='text' name='num' /></td></tr>");
			os.println("<tr><td>Qte</td><td><input type='number' name='qte' /></td></tr>");
			os.println("<tr><td>Date</td><td><input type='date' name='date' /></td></tr>");
			os.println(
					"<tr><td>Client</td><td><select  name='client'> <option value='hidden'>Choisit un client</option>");
			for (Client cl : clientService.getAll()) {
				os.println(String.format(" <option value=%s >%s</option>", cl.getId(), cl.getNom()));
			}
			os.println("</select></tr>");

			os.println("<input type='hidden' name='hidden' value='addCommand' />");
			os.println("<tr><td colspan='2'><input type='submit' value='Add' id='addCommande'/></td></tr>");

		} else {

			Commande cmd = clientService.getCommande(Long.parseLong(update_id));
			os.println("<h2>Modifier cette commande</h2>");

			os.println("<form name='input' action='/clientuirest' method='post'>");
			os.println("<table>");

			String date1 = simpleDateFormat1.format(cmd.getDate());
			os.println("<tr></tr><tr></tr>");
			os.println("<tr><td>Num</td><td><input type='text' name='num' value='" + cmd.getNum() + "'/></td></tr>");
			os.println("<tr><td>Qte</td><td><input type='text' name='qte'  value='" + cmd.getQte() + "'/></td></tr>");
			os.println("<tr><td>Date</td><td><input type='date' name='date' value='" + date1 + "' /></td></tr>");
			os.println("<tr><td>Client</td><td><select  name='client'> <option value='" + cmd.getClient().getId() + "'>"
					+ cmd.getClient().getNom() + "</option>");
			for (Client cl : clientService.getAll()) {
				if (cmd.getClient().getId() != cl.getId()) {
					os.println(String.format(" <option value=%s >%s</option>", cl.getId(), cl.getNom()));
				}
			}
			os.println("</select></tr>");
			os.println("<input type='hidden' name='hidden' value='editCommand' />");
			os.println(
					"<tr><td colspan='2'><input type='submit' value='Edit' id='editCommande'/></td><td><input type='hidden' name='update_id' value='"
							+ update_id + " '  /></td></tr>");
		}

		os.println("</form>");
		os.println("</table>");
		os.println("</body></html>");

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String hidden = req.getParameter("hidden");
		String nom = req.getParameter("nom");
		String prenom = req.getParameter("prenom");
		String email = req.getParameter("email");

		String num = req.getParameter("num");
		int qte = Integer.parseInt(req.getParameter("qte"));
		String date = req.getParameter("date").replace("-", "/");
		String client1 = req.getParameter("client");

		if (hidden.equals("addClient")) {

			Client client = new Client(nom, prenom, email);
			clientService.addClient(client);

		} else if (hidden.equals("addCommand")) {

			clientService.addCommande(
					new Commande(num, qte, new Date(date), clientService.getClient(Long.parseLong(client1))));

		} else if (hidden.equals("editCommand")) {

			clientService.updateCommande(Long.parseLong(update_id),
					new Commande(num, qte, new Date(date), clientService.getClient(Long.parseLong(client1))));
		}

		resp.sendRedirect("/clientuirest/");
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}

}
