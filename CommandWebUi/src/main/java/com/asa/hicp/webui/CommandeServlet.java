package com.asa.hicp.webui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.asa.hicp.imp.ArticleService;
import com.asa.hicp.imp.ClientService;
import com.asa.hicp.model.Article;
import com.asa.hicp.model.Client;
import com.asa.hicp.model.Commande;

public class CommandeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	ArticleService articleService;
	ClientService clientService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ServletOutputStream os = resp.getOutputStream();
		ArrayList<Article> articles = articleService.getAllArticles();
		os.println("<html><body>");
		os.println("<h2>Ajouter votre commande</h2>");
		os.println("<form name='input' action='/commandeuirest' method='post'>");
		os.println("<table>");
		os.println("<tr><td>Num</td><td><input type='text' name='num' /></td></tr>");
		os.println("<tr><td>Qte</td><td><input type='number' name='qte' /></td></tr>");
		os.println("<tr><td>Client</td><td><select  name='client'> <option value='hidden'>Choisit un client</option>");
		for (Client cl : clientService.getAll()) {
			os.println(String.format(" <option value=%s >%s</option>", cl.getId(), cl.getNom()));
		}
		os.println("</select></tr>");
		os.println("</table>");
		os.println("<h3>Choisissez vos articles</h3>");
		os.println("<table>");
		for (Article ar : articles) {
			// if (ar.getCom() == null) {
			os.println(String.format("<tr><td>%s</td><td>%s</td><td><input type='checkbox'  name='checked' value='"
					+ ar.getId() + "'></td></tr>", ar.getDesignation(), ar.getPrix()));
			// }
		}
		os.println("<tr><td colspan='2'><input type='submit' value='Commander' /></td></tr>");
		os.println("</table>");
		os.println("</form>");

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String[] liste = req.getParameterValues("checked");
		String num = req.getParameter("num");
		int qte = Integer.parseInt(req.getParameter("qte"));
		String client = req.getParameter("client");

		Commande commande = clientService.addAndReturnCommande(
				new Commande(num, qte, new Date(), clientService.getClient(Long.parseLong(client))));

		for (int i = 0; i < liste.length; i++) {
			Article art = articleService.getArticle(Long.parseLong(liste[i]));
			art.setCom(commande);
			articleService.updateArticle(Long.parseLong(liste[i]), art);
		}

		resp.sendRedirect("/commandeuirest");
	}

	public ArticleService getArticleService() {
		return articleService;
	}

	public void setArticleService(ArticleService articleService) {
		this.articleService = articleService;
	}

	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}

}
