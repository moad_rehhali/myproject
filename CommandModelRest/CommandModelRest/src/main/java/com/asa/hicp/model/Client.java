package com.asa.hicp.model;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement

public class Client implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "nom", length = 100)
	private String nom;

	@Column(name = "prenom", length = 100)
	private String prenom;

	@Column(name = "adresse", length = 200)
	private String adr;

	@Column(name = "email", length = 100)
	private String email;

	@Column(name = "tel", length = 100)
	private String tel;

	@OneToMany(mappedBy = "Commande", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Transient
	private Collection<Commande> commands;

	public Client() {

	}

	public Client(Long id, String nom, String prenom, String adr, String email, String tel,
			Collection<Commande> commands) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.adr = adr;
		this.email = email;
		this.tel = tel;
		this.commands = commands;
	}

	public Client(Long id, String nom, String prenom, String email) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
	}

	public Client(String nom, String prenom, String email) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdr() {
		return adr;
	}

	public void setAdr(String adr) {
		this.adr = adr;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public Collection<Commande> getCommands() {
		return commands;
	}

	public void setCommands(Collection<Commande> commands) {
		this.commands = commands;
	}

}
