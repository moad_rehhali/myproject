package com.asa.hicp.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@XmlRootElement
public class Commande implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "num", length = 100)
	private String num;

	@Column(name = "qte")
	private Integer qte;

	@Column(name = "date", length = 100)
	@Temporal(TemporalType.DATE)
	private Date date;

	@ManyToOne(fetch = FetchType.EAGER)
	private Client client;

	@OneToMany(mappedBy = "Article", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Transient
	private Collection<Article> articles;

	public Commande() {
	}

	public Commande(Long id, String num, Integer qte, Date date, Client c) {
		super();
		this.id = id;
		this.num = num;
		this.qte = qte;
		this.date = date;
		this.client = c;
	}

	public Commande(String num, Integer qte, Date date, Client client) {
		super();
		this.num = num;
		this.qte = qte;
		this.date = date;
		this.client = client;
	}

	public Commande(String num, Integer qte, Date date) {
		super();
		this.num = num;
		this.qte = qte;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public Integer getQte() {
		return qte;
	}

	public void setQte(Integer qte) {
		this.qte = qte;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Collection<Article> getArticles() {
		return articles;
	}

	public void setArticles(Collection<Article> articles) {
		this.articles = articles;
	}

}