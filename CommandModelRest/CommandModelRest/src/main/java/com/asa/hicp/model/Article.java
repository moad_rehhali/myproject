package com.asa.hicp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@XmlRootElement
public class Article implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "designation", length = 100)
	private String designation;

	@Column(name = "prix")
	private Double prix;

	@ManyToOne
	private Commande com;

	@ManyToOne
	private Categorie categorie;

	@ManyToOne
	private Fournisseur fournisseur;

	public Article() {
	}

	public Article(Long id, String designation, Double prix, Commande com, Categorie cat) {
		super();
		this.id = id;
		this.designation = designation;
		this.prix = prix;
		this.com = com;
		this.categorie = cat;
	}

	public Article(String designation, Double prix, Categorie categorie, Fournisseur fournisseur) {
		super();
		this.designation = designation;
		this.prix = prix;
		this.categorie = categorie;
		this.fournisseur = fournisseur;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Double getPrix() {
		return prix;
	}

	public void setPrix(Double prix) {
		this.prix = prix;
	}

	public Commande getCom() {
		return com;
	}

	public void setCom(Commande com) {
		this.com = com;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

}